// **Iteración #2: Modificando el DOM**

// Dato el siguiente HTML: iteracion2.html

// ```bash
// 2.1 Inserta dinamicamente en un html un div vacio con javascript.
// 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
// 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
// 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.
// 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
// 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
// const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
// 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
// 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
// 	Recuerda que no solo puedes insertar elementos con .appendChild.
// 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here
// ```


window.onload=function(){
    //1
    let newDiv=document.createElement("div");
    document.body.appendChild(newDiv);

    //2
     let newDiv2=document.createElement("div");
     let newP = document.createElement("p");
     newDiv2.appendChild(newP);
     document.body.appendChild(newDiv2);

    //3
    let newDiv3=document.createElement("div");
    let newP3=["p","p","p","p","p","p"];
    for (item of newP3){
        let n= document.createElement(item);
        newDiv3.appendChild(n);
    }
    document.body.appendChild(newDiv3);

    //4
     let newP4 = document.createElement("p");
     newP4.textContent="Soy dinámico";
     document.body.appendChild(newP4);  
    
    //5
    let title =document.createElement("h2");
    title.className="fn-insert-here";
    title.textContent='Wubba Lubba dub dub';
    document.body.appendChild(title);

    //6
    const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
    let lista = document.createElement("ul");
    for ( item of apps){
        let n = document.createElement("li");
        n.textContent=item;
        lista.appendChild(n);
    }
    document.body.appendChild(lista);

    //7
    let del = document.querySelectorAll('.fn-remove-me');
    for (item of del){
        document.body.removeChild(item);
    }

    //8
    let newP8= document.createElement('p');
    newP8.textContent='Voy en medio';
    let divv=document.querySelectorAll('div');
    document.body.insertBefore(newP8,divv[1]);

    //9
    let newP9= document.createElement('p');
    newP9.textContent='Voy dentro';
    let divs= document.querySelectorAll('.fn-insert-here');
    document.body.insertBefore(newP9,divs[2]);




}



